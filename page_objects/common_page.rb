class CommonPage

  include PageObject

  div :flash_notice, id: 'flash_notice'
  span :warning, css: '.warning span'

  link :login, css: '[href="/login"]'
  link :logout, css: '.logout'
  link :my_page, css: 'href="/my/page"'
  link :projects, css: 'href="/projects"'

  button :commit, css: '[name="commit"]'
  button :submit, css: '[type="submit"]'

end