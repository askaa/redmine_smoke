class AccountPage < CommonPage

  page_url URL + '/my/account'

  link :change_password, css: '.icon-passwd'

end