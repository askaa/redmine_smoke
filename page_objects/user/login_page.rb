class LoginPage < CommonPage

  page_url URL + '/login'

  text_field :username, id: 'username'
  text_field :password, id: 'password'

  def login(user, pass)
    self.username = user
    self.password = pass
    submit
    sleep 1
  end

end