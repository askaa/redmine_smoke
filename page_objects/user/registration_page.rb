class RegistrationPage < CommonPage

  page_url URL + '/account/register'

  text_field :user_login, id: 'user_login'
  text_field :user_password, id: 'user_password'
  text_field :user_password_confirmation, id: 'user_password_confirmation'
  text_field :user_firstname, id: 'user_firstname'
  text_field :user_lastname, id: 'user_lastname'
  text_field :user_mail, id: 'user_mail'
  select_list :user_language, id: 'user_language'

  def register_random_user
    credetials = {
        username: Faker::StarWars.character.gsub(' ', '_') + Time.now.to_i.to_s,
        password: 4.times.map{Random.rand(0...9)}.join,
        first_name: Faker::Name.first_name,
        last_name: Faker::Name.last_name,
        email: Faker::Internet.email,
        language: 'English'
    }

    self.user_login = credetials[:username]
    self.user_password = credetials[:password]
    self.user_password_confirmation = credetials[:password]
    self.user_firstname = credetials[:first_name]
    self.user_lastname = credetials[:last_name]
    self.user_mail = credetials[:email]
    self.user_language = credetials[:language]
    submit
    sleep 1
    credetials
  end

end