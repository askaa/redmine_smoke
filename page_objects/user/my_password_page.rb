class MyPasswordPage < CommonPage

  page_url URL + '/my/password'

  text_field :password, id: 'password'
  text_field :new_password, id: 'new_password'
  text_field :new_password_confirmation, id: 'new_password_confirmation'

  def change_user_password(old_password, new_password)
    self.password = old_password
    self.new_password = new_password
    self.new_password_confirmation = new_password
    submit
    new_password
  end

end