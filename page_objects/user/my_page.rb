class MyPage < CommonPage

  page_url URL + '/my/page'

  link :issues, css: '[href*="/issues/"]'

end