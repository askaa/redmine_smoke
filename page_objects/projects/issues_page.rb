class IssuesPage < CommonPage

  include PageObject

  page_url URL + '/issues/'

  link :log_time, css: '[href*="/time_entries/new"]'
  link :edit, css: '[href*="/edit"]'
  text_field :time_entry_spent_on, id: 'time_entry_spent_on'
  text_field :time_entry_hours, id: 'time_entry_hours'
  text_field :time_entry_comments, id: 'time_entry_comments'
  select_list :time_entry_activity, id: 'time_entry_activity_id'
  #----Edit----
  select_list :issue_status, id: 'issue_status_id'
  button :submit, css: '[value="Submit"]'
  td :status, css: 'td.status'

  def add_spent_time
    credetials = {
        hours: 4,
        comment: Faker::Hacker.say_something_smart,
        activity: 'Development'
    }
    wait_until { self.time_entry_spent_on.length > 0 }
    self.time_entry_hours = credetials[:hours]
    self.time_entry_comments = credetials[:comment]
    self.time_entry_activity = credetials[:activity]
    commit
    credetials
  end

  def edit_status_to(status)
    self.issue_status = status
    submit
  end
end