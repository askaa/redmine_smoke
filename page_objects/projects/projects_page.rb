class ProjectsPage < CommonPage

  page_url URL + '/projects'

  link :new_project, css: '.icon-add'
  link :members, css: '[href*="/settings/members"]'
  link :overview, css: '.overview'
  link :close_project, css: '.icon-lock'
  #---Members---
  link :new_member, css: '[href*="/memberships/new"]'
  text_field :principal_search, id: 'principal_search'
  labels :possible_members, css: '#principals label'
  checkbox :add_member, css: '#principals input'
  checkbox :developer_role, css: '.roles-selection [value="4"]'
  button :add, id: 'member-add-submit'
  rows :members_list, css: '.name.user'
  #---Issues----
  link :new_issue, css: '[href*="/issues/new"]'
  text_field :issue_subject, id: 'issue_subject'
  text_area :issue_description, id: 'issue_description'
  select_list :issue_assigned_to , id: 'issue_assigned_to_id'

  def navigate_to_project(name)
    navigate_to "#{URL}/projects/#{name}/settings/"
  end

  def add_new_member(name)
    self.members
    self.new_member
    self.principal_search = name
    wait_until { self.possible_members_elements.length == 1 }
    self.check_add_member
    self.check_developer_role
    self.add
  end

  def create_issue(name = nil)
    credetials = {
        subject: Faker::StarWars.droid + Time.now.to_i.to_s,
        description: Faker::StarWars.quote
    }
    self.issue_subject = credetials[:subject]
    self.issue_description = credetials[:description]
    self.issue_assigned_to = name if name
    self.commit
    wait_until { self.flash_notice.length > 0 }
    credetials
  end

end