class NewProjectPage < CommonPage

  page_url URL + '/projects/new'

  text_field :project_name, id: 'project_name'
  text_area :project_description, id: 'project_description'

  def create_random_project
    credetials = {
        name: Faker::StarWars.droid + Time.now.to_i.to_s,
        description: Faker::StarWars.quote
    }

    self.project_name = credetials[:name]
    self.project_description = credetials[:description]
    commit
    credetials
  end

end