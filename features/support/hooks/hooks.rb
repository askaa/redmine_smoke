Before do
  @browser = Watir::Browser.new BROWSER.to_sym
  @browser.driver.manage.timeouts.implicit_wait = WAIT_TIME
end

After do
  @browser.close
end