require 'bundler'
Bundler.require(:default)
require 'watir-webdriver'
require 'watir-webdriver/wait'
require 'page-object/page_factory'
World(PageObject::PageFactory)
require_relative '../../page_objects/common_page'

#--------------Global Config----------------
MAIN_CONFIG = YAML.load_file("#{Dir.pwd}/config.yml")
URL = ENV['URL'] || MAIN_CONFIG['url']
BROWSER = ENV['BROWSER'] || MAIN_CONFIG['browser']
WAIT_TIME = MAIN_CONFIG['max_wait_time']

Dir["#{Dir.pwd}/page_objects/**/*.rb"].sort.each {|file| require file }

