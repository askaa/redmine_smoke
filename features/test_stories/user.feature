Feature: As user I should have opportunity to register and change my password

  Background: Fill registration field
    Given I visit RegistrationPage
    And I register random user

  @registration
  Scenario: Register new user
    Then I should be redirected to "/my/account" page
    And flash notice should contain "Ваша учётная запись активирована" text

  @change_password
  Scenario: Change password
    And I visit MyPasswordPage
    And I change users password to 12345
    Then I should be redirected to "/my/account" page
    And flash notice should contain "Password was successfully updated" text
