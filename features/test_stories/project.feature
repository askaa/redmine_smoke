Feature: As user I should have opportunity to create new project,
  assign another user as a member and close the project.

  Background: Fill registration field
    Given I visit RegistrationPage
    And I register random user

  @create_project
  Scenario: Create new project
    And I visit ProjectsPage
    And I click New project
    Then I create random project
    Then I should be redirected to "/settings" page
    And flash notice should contain "Successful creation" text

  @assign_member
  Scenario: Assign user as a member for project
    And I remember users login and password
    And I click Logout
    Then I visit RegistrationPage
    And I register random user
    And I visit NewProjectPage
    Then I create random project
    When I assign previous user as a member for a project
    Then I check that user is added to members

  @close_project
  Scenario: Close project
    And I visit ProjectsPage
    And I click New project
    Then I create random project
    And I close project
    And warning should contain "This project is closed and read-only" text

