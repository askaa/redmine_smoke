Feature: As user I should have opportunity to create new issue, assign it to another user.
  As another user I should log time and close assigned issue.

  Background: Fill registration field
    Given I visit RegistrationPage
    And I register random user

  @create_issue
  Scenario: Create a new issue in the following project
    And I remember users login and password
    And I click Logout
    Then I visit RegistrationPage
    And I register random user
    And I visit NewProjectPage
    Then I create random project
    And I assign previous user as a member for a project
    When I create new issue and assign to previous user
    Then flash notice should contain "created" text

  @log_time
  Scenario: Log time for issue
    And I remember users login and password
    And I click Logout
    Then I visit RegistrationPage
    And I register random user
    And I visit NewProjectPage
    Then I create random project
    And I assign previous user as a member for a project
    When I create new issue and assign to previous user
    And I click Logout
    And I click Login
    And I login as previous user
    And I visit MyPage
    And I click Issues
    And I log time for this issue
    Then flash notice should contain "Successful creation" text

  @close_issue
  Scenario: Status closed for issue
    And I remember users login and password
    And I click Logout
    Then I visit RegistrationPage
    And I register random user
    And I visit NewProjectPage
    Then I create random project
    And I assign previous user as a member for a project
    When I create new issue and assign to previous user
    And I click Logout
    And I click Login
    And I login as previous user
    And I visit MyPage
    And I click Issues
    And I edit issue make status "Closed"
    Then flash notice should contain "Successful update" text
    Then status should contain "Closed" text