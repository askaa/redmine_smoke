Then(/^I create random project$/) do
  @page = NewProjectPage.new(@browser)
  @project = @page.create_random_project
end

And(/^I assign previous user as a member for a project$/) do
  @page = ProjectsPage.new(@browser)
  full_name = @old_user_info[:last_name] + ' ' + @old_user_info[:first_name]
  @page.add_new_member full_name
end

Then(/^I check that user is added to members$/) do
  @page.wait_until { @page.members_list_elements.length == 2 }
  expect(@page.members_list_elements.length).to eq 2
end

And(/^I close project$/) do
  @page = ProjectsPage.new(@browser)
  @page.overview
  @page.close_project
  sleep 1
  @browser.alert.ok
end