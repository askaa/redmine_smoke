And(/^I register random user$/) do
  @user_info = @page.register_random_user
end

And(/^I change users password to (\d+)$/) do |new_password|
  @page.change_user_password(@user_info[:password], new_password)
end

And(/^I remember users login and password$/) do
  @old_user_info = @user_info
end

And(/^I login as previous user$/) do
  @page = LoginPage.new(@browser)
  @page.login(@old_user_info[:username], @old_user_info[:password])
end