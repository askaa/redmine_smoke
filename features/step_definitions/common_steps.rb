Given(/^I visit (\w+)/) do |page|
  @page = Object.const_get(page.gsub(" ","")).new(@browser)
  @page.goto
end

Then(/^I should be redirected to "([^"]*)" page$/) do |site|
  6.times do
    break if @browser.url.include? site
    sleep(0.5)
  end
  expect(@browser.url).to include(site)
end


#----------universal-----------

And(/^I click ([\w ]+)$/) do |object|
  object = object.gsub(' ', '_').downcase
  @page.send(object.to_sym)
end

And(/^I fill inputs with following data:$/) do |table|
  data_hash = table.hashes[0]
  data_hash.each do |selector, input_data|
    raw_selector = selector.gsub(' ', '_').downcase.to_sym
    @page.send("#{raw_selector}=", input_data)
  end
end

Then(/^([\w ]+) should contain "([^"]*)" text$/) do |block, expected_text|
  block = block.gsub(' ', '_').downcase
  expect(@page.send block).to include expected_text
end