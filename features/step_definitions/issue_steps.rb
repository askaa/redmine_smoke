And(/^I create new issue and assign to previous user$/) do
  @page = ProjectsPage.new(@browser)
  sleep 1
  @page.new_issue
  full_name = @old_user_info[:first_name] + ' ' + @old_user_info[:last_name]
  @page.create_issue full_name
end

And(/^I log time for this issue$/) do
  @page = IssuesPage.new(@browser)
  @page.log_time
  @page.add_spent_time
end

And(/^I edit issue make status "([^"]*)"$/) do |status|
  @page = IssuesPage.new(@browser)
  @page.edit
  @page.edit_status_to status
end
